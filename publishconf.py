# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Pelican configuration
#######################
AUTHOR   = u'spice-team'
SITENAME = u'SPICE'
SITEURL  = 'https://www.spice-space.org'
THEME    = 'spice-space'
TIMEZONE = 'Europe/Paris'
PATH     = 'content'

DEFAULT_LANG  = u'en'
RELATIVE_URLS = False

# Static content that we reference in our site goes into folders appointed
# by this path
STATIC_PATHS = ['static', 'download']

# Templates that are used directly to render content
DIRECT_TEMPLATES = ['search']

# ATOM/RSS not used. Disable so they are not generated (and not warned)
FEED_DOMAIN = TRANSLATION_FEED_ATOM = None

FEED_ATOM          = FEED_RSS          = None
FEED_ALL_ATOM      = FEED_ALL_RSS      = None
CATEGORY_FEED_ATOM = CATEGORY_FEED_RSS = None
AUTHOR_FEED_ATOM   = AUTHOR_FEED_RSS   = None
TAG_FEED_ATOM      = TAG_FEED_RSS      = None


PAGE_URL         = PAGE_SAVE_AS         = '{slug}.html'
PAGE_LANG_URL    = PAGE_LANG_SAVE_AS    = '{lang}/{slug}.html'
ARTICLE_URL      = ARTICLE_SAVE_AS      = '{slug}.html'
ARTICLE_LANG_URL = ARTICLE_LANG_SAVE_AS = '{lang}/{slug}.html'
CATEGORY_URL     = CATEGORY_SAVE_AS     = None

# Theme-related configuration
#############################
DEFAULT_PAGINATION = False
FOOTER_IMAGE_PATH = SITEURL + '/theme/images/icons/osci_logo.svg'

MENUITEMS = [
	("HOME", "index.html"),
	("FAQ", "faq.html"),
	("Features", "features.html"),
	("Documentation", "documentation.html"),
	("Support", "support.html"),
	("Download", "download.html"),
	("Developers", "developers.html"),
	("Contact", "contact.html"),
]

SUBMENUS = {
	"download" : [
		("Download", "download.html"),
		("Repositories", "repositories.html"),
		("Releases", "releases.html"),
	],
}

# Plugins configuration
#######################
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["asciidoc_reader", "tipue_search", "sitemap"]

SITEMAP = {'format' : 'xml'}

ASCIIDOC_OPTIONS = ["-f asciidoc.conf",
                    "-a iconsdir=" + SITEURL + "/theme/images/icons"]
ASCIIDOC_BACKEND = "html5"
